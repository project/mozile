Introduction:

This module is a WYSIWYG that enables you to edit content inline.

This module allows you to:

1. Edit the body field of a specific content type.
2. It also allows you to edit the blocks created via the block module.

To use this module:

1. Download mozile from http://mozile.mozdev.org/use.html#MozileServerSide inside <path_to_mozile_module>/mozile, as of this writing, the latest is mozile 0.8
   When successful, the file structure after will be <module_folder>/mozile/index.html
2. Enable the module.
3. Check permissions.
4. Edit the content type to enable this module for specific content types.
5. For blocks, create a block using the administration interface, then edit the block to enable the module for it.
6. Be aware that this module will save the result of the input filters back into the node body. You can't be sure that the contents of the body field will be saved as they were unless the input filter doesn't modify the body content.

NOTE: This module works best with XHTML. If you face problems, make sure to begin your block or node with XHTML text. E.g. <p>BEGIN</p>
NOTE: This module is in beta stage. Your comments are welcome.